unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls,
  Vcl.Imaging.jpeg;

type
  TBandas = class(TForm)
    listbandas: TListBox;
    Inserir: TButton;
    Deletar: TButton;
    Salvar: TButton;
    Carregar: TButton;
    Atualizar: TButton;
    enome: TEdit;
    egenero: TEdit;
    eano: TEdit;
    ImgBandas: TImage;
    procedure listbandasClick(Sender: TObject);
    procedure InserirClick(Sender: TObject);
    procedure AtualizarClick(Sender: TObject);
    procedure DeletarClick(Sender: TObject);
    procedure SalvarClick(Sender: TObject);
    procedure CarregarClick(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;
     TBanda = class(TObject)
    nome, genero, ano: string;
    end;
var
  Bandas: TBandas;

implementation

{$R *.dfm}

procedure TBandas.AtualizarClick(Sender: TObject);
var Banda2:TBanda;
var nome, genero, ano: string;
begin
  nome:=enome.Text;
  genero:=egenero.Text;
  ano:=eano.Text;
  if (nome.Length<>0) and (genero.Length<>0) and (ano.Length<>0) then
  begin
    Banda2 := listbandas.Items.Objects[listbandas.ItemIndex] as Tbanda;
    Banda2.nome := nome;
    Banda2.genero := genero;
    Banda2.ano := ano;
    listbandas.Items[listbandas.ItemIndex]:=enome.Text ;
  end
  else
  begin
    ShowMessage('H� campos em branco');
  end;
end;





procedure TBandas.CarregarClick(Sender: TObject);
var arqBandas:TextFile;
var Banda4:TBanda;
begin
  AssignFile(arqBandas, 'Bandas.txt');
  Reset(arqBandas);

  Banda4:=TBanda.Create;

  while not Eof(arqBandas) do
  begin
      Readln(arqBandas, Banda4.nome);
      Readln(arqBandas,Banda4.genero);
      Readln(arqBandas, Banda4.ano);

      listbandas.Items.AddObject(Banda4.nome, Banda4);
  end;
    CloseFile(arqBandas);
end;

procedure TBandas.DeletarClick(Sender: TObject);
begin
listbandas.DeleteSelected;
end;

procedure TBandas.InserirClick(Sender: TObject);
var bandas:TBanda;
var nome, genero, ano: string;
begin
bandas:=TBanda.Create;
bandas.nome:=enome.Text;
bandas.genero:=egenero.Text;
bandas.ano:=eano.Text;
if (bandas.nome <> '') and (bandas.genero <> '') and (bandas.ano <> '')then
begin
listbandas.items.AddObject(bandas.nome, bandas);
enome.text:='';
egenero.text:= '';
eano.text:='';
end else
begin
ShowMessage('Campo vazio');
end;
end;

procedure TBandas.listbandasClick(Sender: TObject);
begin
Deletar.Enabled:=true;
Atualizar.Enabled:=true;
end;

procedure TBandas.SalvarClick(Sender: TObject);
var arqBandas : TextFile;
var Banda3:TBanda;
var i: integer;
begin
    if listbandas.Items.Count<>0 then
    begin
      AssignFile(arqBandas, 'Bandas.txt');
      Rewrite(arqBandas);
      for i:=0 to Pred(listbandas.Items.Count) do
      begin
        banda3 := listbandas.Items.Objects[i] as TBanda;
        writeln(arqBandas, Banda3.nome);
        writeln(arqBandas, Banda3.genero);
        writeln(arqBandas, Banda3.ano);
      end;

      CloseFile(arqBandas);
    end
    else
    begin
      ShowMessage('ERRO! Nada para salvar');
    end;

end;
end.
